﻿using TFGServer.Networking;

namespace TFGServer.Main {
    class ServerMain {
        static void Main() {
            Server server = new Server();
            server.Start();
        }
    }
}
