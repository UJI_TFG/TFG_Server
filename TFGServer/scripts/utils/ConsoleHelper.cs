﻿using System;

namespace TFGServer.Utils {
    class ConsoleHelper {
        public static void WriteLine(string message) {
            Console.ResetColor();
            Console.WriteLine(" ** " + message);
        }

        public static void SentMessageLine(string message) {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(" >> " + message);
            Console.ResetColor();
        }
        public static void ReceivedMessageLine(string message) {
            Console.ForegroundColor = ConsoleColor.Yellow; 
            Console.WriteLine(" << " + message);
            Console.ResetColor();
        }

        public static void WriteLine(string message, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(" ** " + message);
            Console.ResetColor();
        }

        public static void Write(string message, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.Write(" ** " + message);
            Console.ResetColor();
        }

    }
}
