﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using TFGServer.Utils;
using TFGCommonLib.Networking;
using TFGCommonLib.Utils;
using TFGCommonLib.Threading;
using System.IO;

namespace TFGServer.Networking {

    class ClientHandler : ConnectionProcessor {
        private Socket clientSocket;
        private string clientIP;

        private int userID;

        private Stack<string> downloadGameData;

        private List<string> commandList = new List<string>();

        public void StartClient(Socket inClientSocket) {
            clientSocket = inClientSocket;
            clientIP = ((IPEndPoint)clientSocket.RemoteEndPoint).Address.ToString();
            Thread clientThread = new Thread(processRequests);
            clientThread.Start();
        }

        private void processRequests() {

            while (clientSocket.Connected) {
                try {
                    receive();
                    while (commandList.Count > 0) {
                        string message = commandList[0];
                        commandList.RemoveAt(0);

                        OpCode opcode = MessageHandler.ReadOpCode(message);

                        switch (opcode) {
                            case OpCode.LOGIN:
                                performLogin(message);
                                break;

                            case OpCode.GET_VERSION:
                                int[] version = DatabaseManager.Instance.GetVersion();
                                Send(OpCode.VERSION, version[0].ToString(), version[1].ToString(), version[2].ToString());
                                break;

                            case OpCode.DOWNLOAD_CLIENT:
                                prepareDownload();
                                break;

                            case OpCode.TRANSFER_PREPARED:
                                downloadClient();
                                break;

                            case OpCode.UPDATE_USER:
                                updateUser(message);
                                break;

                            case OpCode.CREATE_MATCH:
                                createMatch(message);
                                break;

                            case OpCode.UPDATE_MATCH:
                                updateMatch(message);
                                break;

                            case OpCode.ADD_MATCH_PLAYER:
                                addPlayerToMatch(message);
                                break;

                            case OpCode.UPDATE_MATCH_PLAYER:
                                updateMatchPlayer(message);
                                break;

                            case OpCode.REQUEST_EXP_RATE:
                                sendExpRate();
                                break;

                            default:
                            case OpCode.DISCONNECT:
                                disconnect();
                                break;
                        }
                    }
                }
                catch (Exception ex) {
                    Send(ErrorCode.SERVER_ERROR);
                    ConsoleHelper.WriteLine(ex.ToString(), ConsoleColor.Red);
                    disconnect();
                }
            }
        }

        private void performLogin(string message) {
            string[] loginInput = MessageHandler.ReadParams(message);
            userID = DatabaseManager.Instance.CheckIfUserExists(loginInput[0], loginInput[1]);
            if (userID != -1) {
                DateTime? banned = DatabaseManager.Instance.CheckIfUserIsBanned(userID);
                if (banned == null) {
                    int exp = DatabaseManager.Instance.GetUserExp(userID);
                    Send(OpCode.LOGIN_OK, userID.ToString(), exp.ToString());
                    DatabaseManager.Instance.SetPlayerOnlineStatus(userID, true);
                }
                else {
                    if(banned.Value.CompareTo(DateTime.Now) <= 0) {
                        DatabaseManager.Instance.Unban(userID);
                        performLogin(message);
                    }
                    else {
                        Send(ErrorCode.BANNED_USER, DateTimeUtils.DateTimeToTimestamp(banned.Value).ToString());
                        disconnect();
                    }
                }
            }
            else {
                Send(ErrorCode.WRONG_LOGIN);
                disconnect();
            }
        }

        private void prepareDownload() {
            string root = "GameClient";

            downloadGameData = traverseTree(root);

            Send(OpCode.TRANSFER_INFO, downloadGameData.Count.ToString());
        }

        private void downloadClient() {
            bool prepared = true;
            bool finished = true;
            string file = "";

            while (downloadGameData.Count > 0) {

                if (prepared && finished) {
                    file = downloadGameData.Pop();

                    FileInfo fi = new FileInfo(file);
                    Send(OpCode.SEND_FILE, file.Replace(' ', '*'), fi.Length.ToString());
                    prepared = false;
                    finished = false;
                }

                string msg = null;

                while (msg == null) {
                    receive();
                    for (int i = 0; i < commandList.Count; i++) {
                        if (MessageHandler.ReadOpCode(commandList[i]) == OpCode.TRANSFER_PREPARED ||
                            MessageHandler.ReadOpCode(commandList[i]) == OpCode.TRANSFER_FINISHED) {
                            msg = commandList[i];
                            commandList.RemoveAt(i);
                            break;
                        }
                    }
                }

                if (MessageHandler.ReadOpCode(msg) == OpCode.TRANSFER_PREPARED) {
                    prepared = true;
                    ConsoleHelper.WriteLine("Sending " + file, ConsoleColor.DarkGreen);

                    clientSocket.SendFile(file);
                }
                else if (MessageHandler.ReadOpCode(msg) == OpCode.TRANSFER_FINISHED) {
                    finished = true;
                }
            }
        }

        private void updateUser(string message) {
            string[] data = MessageHandler.ReadParams(message);

            DatabaseManager.Instance.UpdateUser(userID, int.Parse(data[0]));
        }

        private void createMatch(string message) {
            string[] data = MessageHandler.ReadParams(message);

            int matchID = DatabaseManager.Instance.CreateMatch(int.Parse(data[0]));

            Send(OpCode.MATCH_ID, matchID.ToString());
        }

        private void addPlayerToMatch(string message) {
            string[] data = MessageHandler.ReadParams(message);

            DatabaseManager.Instance.AddPlayerToMatch(int.Parse(data[0]), userID);
        }

        private void updateMatchPlayer(string message) {
            string[] data = MessageHandler.ReadParams(message);

            int matchID = int.Parse(data[0]);
            int score = int.Parse(data[1]);
            int elim = int.Parse(data[2]);
            int deaths = int.Parse(data[3]);

            //ConsoleHelper.writeLineConsoleMessage(string.Format("{0}, {1}, {2}, {3}, {4}", userID, matchID, score, elim, deaths));

            DatabaseManager.Instance.UpdateMatchPlayer(userID, matchID, score, elim, deaths);
        }

        private void updateMatch(string message) {
            string[] data = MessageHandler.ReadParams(message);

            int matchID = int.Parse(data[0]);
            DateTime endDate = DateTimeUtils.TimestampToDateTime(double.Parse(data[1]));
            int winner = int.Parse(data[2]);

            DatabaseManager.Instance.UpdateMatch(matchID, endDate, winner);
        }

        private void sendExpRate() {
            int expRate = DatabaseManager.Instance.GetExpRate();
            Send(OpCode.EXP_RATE, expRate.ToString());
        }

        private Stack<string> traverseTree(string root) {
            Stack<string> allFilePaths = new Stack<string>();
            Stack<string> dirs = new Stack<string>(20);

            if (!Directory.Exists(root)) {
                throw new ArgumentException();
            }
            dirs.Push(root);

            while (dirs.Count > 0) {
                string currentDir = dirs.Pop();
                string[] subDirs;
                try {
                    subDirs = Directory.GetDirectories(currentDir);
                }
                catch (UnauthorizedAccessException e) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                catch (DirectoryNotFoundException e) {
                    Console.WriteLine(e.Message);
                    continue;
                }

                string[] files = null;
                try {
                    files = Directory.GetFiles(currentDir);
                }
                catch (UnauthorizedAccessException e) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                catch (DirectoryNotFoundException e) {
                    Console.WriteLine(e.Message);
                    continue;
                }

                foreach (string file in files) {
                    try {
                        FileInfo fi = new FileInfo(file);
                        //Console.WriteLine("{0}\\{1}", currentDir, fi.Name);
                        allFilePaths.Push(string.Format("{0}\\{1}", currentDir, fi.Name));
                    }
                    catch (FileNotFoundException e) {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }

                foreach (string str in subDirs)
                    dirs.Push(str);
            }

            return allFilePaths;
        }

        public override void Send(ErrorCode errorCode, params string[] args) {
            string message = MessageHandler.CreateMessage(errorCode, args);
            ConsoleHelper.SentMessageLine(message);
            send(message);
        }

        public override void Send(OpCode opCode, params string[] args) {
            string message = MessageHandler.CreateMessage(opCode, args);
            ConsoleHelper.SentMessageLine(message);
            send(message);
        }

        protected override void send(string message) {
            byte[] sendBytes = Encoding.ASCII.GetBytes(message);

            if (clientSocket.Connected)
                clientSocket.Send(sendBytes, sendBytes.Length, SocketFlags.None);
        }

        protected override void receive() {
            byte[] bytesFrom = new byte[1024];

            if (clientSocket.Connected) {
                clientSocket.Receive(bytesFrom);

                string message = Encoding.ASCII.GetString(bytesFrom).TrimEnd((char)0);

                if (!message.Equals("")) {
                    string[] commands = message.Split('\n');

                    foreach (string command in commands) {
                        if (command != "") {
                            commandList.Add(command);
                            ConsoleHelper.ReceivedMessageLine(command);
                        }
                    }
                        
                }
            }
        }

        public override string Receive() {
            throw new NotImplementedException();
        }

        private void disconnect() {
            clientSocket.Shutdown(SocketShutdown.Both);
            clientSocket.Close();
            if (userID != -1)
                DatabaseManager.Instance.SetPlayerOnlineStatus(userID, false);
            ConsoleHelper.WriteLine("Client disconnected: " + clientIP);

            Dispatcher.Instance.Invoke(() => {
                Server.ReduceConnections();
            });

        }
    }
}
