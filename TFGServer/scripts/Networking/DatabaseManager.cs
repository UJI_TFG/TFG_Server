﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using TFGServer.Utils;

namespace TFGServer.Networking {
    class DatabaseManager {

        private static NpgsqlConnection conn;

        private static DatabaseManager instance;

        public static DatabaseManager Instance {
            get {
                if (instance == null)
                    instance = new DatabaseManager();
                return instance;
            }
        }

        public void CreateConnection(string ip) {
            conn = new NpgsqlConnection("Server=" + ip + ";Port=5432;User Id=tfgtest; " +
             "Password=tfgtest;Database=tfg_database;Pooling=true;MinPoolSize=1;MaxPoolSize=50;ConnectionLifeTime = 10;");
        }

        /// <summary>
        /// Checks if the combination of username and password exists on the database.
        /// </summary>
        /// <param name="user">Username from client</param>
        /// <param name="pass">Password from client</param>
        /// <returns>Returns the user_id if the user is registered, or -1 if not.</returns>
        public int CheckIfUserExists(string user, string pass) {

            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "SELECT user_id FROM USERS WHERE upper(username) = upper(:user) AND upper(password) = upper(:pass);";
            command.Parameters.AddWithValue(":user", user);
            command.Parameters.AddWithValue(":pass", pass);
            command.Connection = conn;

            conn.Open();
            object result = command.ExecuteScalar();
            conn.Close();

            int id = -1;

            if (result != null)
                id = (int)result;
            return id;
        }

        /// <summary>
        /// Checks if the account with 'user_id' is banned.
        /// </summary>
        /// <param name="user_id">user_id from an account</param>
        /// <returns>Returns null if the user is not banned, and a date if is.</returns>
        public DateTime? CheckIfUserIsBanned(int user_id) {

            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "SELECT banned_until FROM USERS WHERE user_id = :user;";
            command.Parameters.AddWithValue(":user", user_id);
            command.Connection = conn;

            conn.Open();
            object result = command.ExecuteScalar();
            conn.Close();

            if (result == DBNull.Value) {
                return null;
            }
            return (DateTime?)result;
        }

        public void SetPlayerOnlineStatus(int playerID, bool online) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "UPDATE users SET isonline = :status WHERE user_id = :pID;";
            command.Parameters.AddWithValue(":pID", playerID);
            command.Parameters.AddWithValue(":status", online);
            command.Connection = conn;

            conn.Open();
            command.ExecuteScalar();
            conn.Close();
        }

        public int GetUserExp(int userID) {

            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "SELECT experience FROM users WHERE user_id = :user;";
            command.Parameters.AddWithValue(":user", userID);
            command.Connection = conn;

            conn.Open();
            object result = command.ExecuteScalar();
            conn.Close();

            int exp = -1;

            if (result != null)
                exp = (int)result;
            return exp;
        }

        public int[] GetVersion() {

            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "SELECT v_major, v_minor, v_revision FROM game_data;";
            command.Connection = conn;

            int[] result = new int[3];

            conn.Open();
            NpgsqlDataReader dbReader = command.ExecuteReader();
            dbReader.Read();

            for (int i = 0; i < 3; i++)
                result[i] = dbReader.GetInt16(i);

            dbReader.Close();
            conn.Close();

            return result;
        }

        public void Unban(int userID) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "UPDATE users SET banned_until = NULL WHERE user_id = :uID;";
            command.Parameters.AddWithValue(":uID", userID);
            command.Connection = conn;

            conn.Open();
            command.ExecuteScalar();
            conn.Close();
        }

        public void UpdateUser(int userID, int exp) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "UPDATE users SET experience = :exp WHERE user_id = :uID;";
            command.Parameters.AddWithValue(":uID", userID);
            command.Parameters.AddWithValue(":exp", exp);
            command.Connection = conn;

            conn.Open();
            command.ExecuteScalar();
            conn.Close();
        }

        public int CreateMatch(int gameModeID) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = @"INSERT INTO matches (start_date, gamemode_id) VALUES (CURRENT_TIMESTAMP, :gmID);
            SELECT currval(pg_get_serial_sequence('matches', 'match_id'));";
            command.Parameters.AddWithValue(":gmID", gameModeID);
            command.Connection = conn;

            conn.Open();
            object matchID = command.ExecuteScalar();
            conn.Close();

            ConsoleHelper.WriteLine(string.Format("Match #{0} created!", matchID), ConsoleColor.DarkGreen);

            return int.Parse(matchID.ToString());
        }

        /*public void UpdateMatch(int matchId, int endDate, int winner) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "UPDATE matchdes SET endDate = :end, winner_team = :win WHERE match_id = :mID;";
            command.Parameters.AddWithValue(":end", endDate);
            command.Parameters.AddWithValue(":win", winner);
            command.Parameters.AddWithValue(":mID", matchId);
            command.Connection = conn;

            conn.Open();
            command.ExecuteScalar();
            conn.Close();
        }*/

        public void AddPlayerToMatch(int matchID, int userID) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "INSERT INTO plays (user_id, match_id) VALUES (:uID, :mID);";
            command.Parameters.AddWithValue(":uID", userID);
            command.Parameters.AddWithValue(":mID", matchID);
            command.Connection = conn;

            try {
                conn.Open();
                command.ExecuteScalar();
                conn.Close();
                ConsoleHelper.WriteLine(string.Format("Added player #{0} to match #{1}", userID, matchID), ConsoleColor.DarkGreen);
            }
            catch (Exception e) {
                conn.Close();
                ConsoleHelper.WriteLine(string.Format("Player #{0} is in match #{1}", userID, matchID), ConsoleColor.DarkGreen);
            }
        }

        public void UpdateMatch(int matchID, DateTime endDate, int winner) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "UPDATE matches SET end_date = :end, winner_team = :winner WHERE match_id = :mID;";
            command.Parameters.AddWithValue(":mID", matchID);
            command.Parameters.AddWithValue(":end", endDate);
            command.Parameters.AddWithValue(":winner", winner);
            command.Connection = conn;

            conn.Open();
            command.ExecuteScalar();
            conn.Close();
        }

        public int GetExpRate() {

            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "SELECT exp_rate FROM game_data;";
            command.Connection = conn;

            conn.Open();
            object result = command.ExecuteScalar();
            conn.Close();

            int expRate = 1;

            if (result != null)
                expRate = (int)result;
            return expRate;
        }

        public void UpdateMatchPlayer(int userID, int matchID, int score, int eliminations, int deaths) {
            NpgsqlCommand command = new NpgsqlCommand();
            command.CommandText = "UPDATE plays SET personal_score = :score, eliminations = :elim, deaths = :deaths WHERE match_id = :mID AND user_id = :uID;";
            command.Parameters.AddWithValue(":uID", userID);
            command.Parameters.AddWithValue(":mID", matchID);
            command.Parameters.AddWithValue(":score", score);
            command.Parameters.AddWithValue(":elim", eliminations);
            command.Parameters.AddWithValue(":deaths", deaths);
            command.Connection = conn;

            conn.Open();
            command.ExecuteScalar();
            conn.Close();
        }
    }
}
