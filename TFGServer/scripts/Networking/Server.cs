﻿using System;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using TFGServer.Utils;
using TFGCommonLib.Utils;
using TFGCommonLib.Threading;

namespace TFGServer.Networking {
    class Server {

        private string configName = "ServerConfig.ini";
        private string serverIP;
        private int serverPort;
        private string databaseIP;
        private volatile bool serverRunning = false;
        private volatile bool serverRunningCommands = false;
        private static int numConnections = 0;
        private int totalConnections = 0;
        private static readonly object obj = new object();

        private DateTime startedDate;

        Thread commandThread;
        Thread workerThread;
        Thread dispatcherThread;

        Socket clientSocket;
        Socket serverSocket;

        public Server() {
            if (hasConfig())
                readConfigFile();
            else
                createConfigFile();

            DatabaseManager.Instance.CreateConnection(databaseIP);
            ConsoleHelper.WriteLine("Server\t= " + serverIP + ":" + serverPort);
            ConsoleHelper.WriteLine("Database\t= " + databaseIP);

            //ConsoleHelper.WriteLine("Type START command to start the server.", ConsoleColor.Cyan);

            commandThread = new Thread(processConsoleCommands);
            commandThread.Start();

        }

        public void Start() {
            lock (obj) {
                serverRunning = true;
            }

            workerThread = new Thread(processConnections);
            workerThread.Start();

            dispatcherThread = new Thread(dispatcherProcessor);
            dispatcherThread.Start();

            startedDate = DateTime.Now;
        }

        public static void ReduceConnections() {
            lock (obj) {
                numConnections--;
            }
        }

        private bool hasConfig() {
            return System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\" + configName);
        }

        private void createConfigFile() {
            ConsoleHelper.WriteLine("Initial config", ConsoleColor.Magenta);
            ConsoleHelper.WriteLine("[Connection]", ConsoleColor.DarkMagenta);
            ConsoleHelper.Write("Server ip: ", ConsoleColor.White);
            serverIP = Console.ReadLine();
            ConsoleHelper.Write("Server port: ", ConsoleColor.White);
            serverPort = int.Parse(Console.ReadLine());
            ConsoleHelper.WriteLine("[Database]", ConsoleColor.DarkMagenta);
            ConsoleHelper.Write("Database ip: ", ConsoleColor.White);
            databaseIP = Console.ReadLine();

            INIFile iniFile = new INIFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + configName);
            iniFile.Write("Connection", "ip", serverIP);
            iniFile.Write("Connection", "port", serverPort.ToString());
            iniFile.Write("Database", "ip", databaseIP);

            ConsoleHelper.WriteLine("Config file created successfully!", ConsoleColor.Green);
        }

        private void readConfigFile() {
            INIFile iniFile = new INIFile(AppDomain.CurrentDomain.BaseDirectory + "\\" + configName);
            serverIP = iniFile.Read("Connection", "ip");
            serverPort = int.Parse(iniFile.Read("Connection", "port"));
            databaseIP = iniFile.Read("Database", "ip");
        }

        private bool isRunning() {
            return serverRunning;
        }

        private bool isRunningCommands() {
            return serverRunningCommands;
        }

        private void stopServer() {
            ConsoleHelper.WriteLine("Stopping server.", ConsoleColor.Cyan);
            lock(obj) {
                //serverRunningCommands = false;
                serverRunning = false;
            }

            try {
                serverSocket.Shutdown(SocketShutdown.Both);
            }
            catch (Exception e) {}
            serverSocket.Close();
        }

        private void processConnections() {
            ConsoleHelper.WriteLine("Server started!", ConsoleColor.Green);

            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            clientSocket = default(Socket);

            IPAddress ipAddress = IPAddress.Parse(serverIP);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, serverPort);

            serverSocket.Bind(remoteEP);
            serverSocket.Listen(10);

            try {
                while (isRunning()) {
                    clientSocket = serverSocket.Accept();
                    ConsoleHelper.WriteLine("Client accepted: " + ((IPEndPoint)clientSocket.RemoteEndPoint).Address.ToString());
                    ClientHandler client = new ClientHandler();
                    client.StartClient(clientSocket);
                    lock (obj) {
                        numConnections++;
                    }
                    totalConnections++;
                }
            }
            catch (Exception e) {
                ConsoleHelper.WriteLine("Server socket closed.", ConsoleColor.Red);
            }

            ConsoleHelper.WriteLine("Server closed.", ConsoleColor.Yellow);
        }

        private void processConsoleCommands() {
            lock (obj) {
                serverRunningCommands = true;
            }

            ConsoleHelper.WriteLine("Console command listener started.", ConsoleColor.Green);
            while (isRunningCommands()) {
                string command = Console.ReadLine();
                if (command.ToLower().Equals("help")) {
                    ConsoleHelper.WriteLine("LIST OF COMMANDS", ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine("help:\tshows this message.", ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine("start:\tstart the server if is not running.", ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine("stop:\tstops the server if is running.", ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine("exit:\texits from the application (stops the server if is running).", ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine("info:\tshows some information about the server.", ConsoleColor.Cyan);
                    //ConsoleHelper.WriteLine("register");
                }
                else if (command.ToLower().Equals("stop")) {
                    if (serverRunning)
                        stopServer();
                    else
                        ConsoleHelper.WriteLine("Server is not running!", ConsoleColor.Red);
                }
                else if (command.ToLower().Equals("exit")) {
                    ConsoleHelper.WriteLine("Server closed forcibly.", ConsoleColor.Red);
                    Environment.Exit(0);
                }
                else if (command.ToLower().Equals("info")) {
                    ConsoleHelper.WriteLine("Server\t= " + serverIP + ":" + serverPort, ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine("Database\t= " + databaseIP, ConsoleColor.Cyan);
                    lock (obj) {
                        ConsoleHelper.WriteLine(string.Format("Clients connected right now: {0}", numConnections), ConsoleColor.Cyan);
                    }
                    ConsoleHelper.WriteLine(string.Format("Total of clients connected: {0}", totalConnections), ConsoleColor.Cyan);
                    ConsoleHelper.WriteLine(string.Format("Server up {0}:{1:00}", (DateTime.Now - startedDate).Minutes, (DateTime.Now - startedDate).Seconds), ConsoleColor.Cyan);
                }
                else if (command.ToLower().Equals("start")) {
                    if (!serverRunning)
                        Start();
                    else
                        ConsoleHelper.WriteLine("Server already running!", ConsoleColor.Red);
                }
                else if (command.ToLower().Contains("register")) {
                    ConsoleHelper.WriteLine("MESSAGE PLACEHOLDER", ConsoleColor.Red);
                }
                else {
                    ConsoleHelper.WriteLine("Unknown command: " + command, ConsoleColor.Red);
                }
            }

            ConsoleHelper.WriteLine("Console command listener finished.", ConsoleColor.Yellow);
        }

        private void dispatcherProcessor() {
            while (serverRunning) {
                Dispatcher.Instance.InvokePending();
            }

            ConsoleHelper.WriteLine("Dispatcher processor finished.", ConsoleColor.Yellow);
        }
    }
}
